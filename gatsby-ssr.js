const React = require("react");
exports.onRenderBody = ({ setPostBodyComponents }) => {
    setPostBodyComponents([
      <script
        dangerouslySetInnerHTML={
          {
            __html: `
              !function() {
                  var config = {
                      id: "maxmilhas-widget",
                      logo: false
                  };
          
                  var t="maxmilhas-widget-js";if(!document.getElementById(t)){var a=document.createElement("script");a.id=t,
                      a.type="text/javascript",a.src="https://widget.maxmilhas.com.br/v2/js/widget.js",a.async=!0,a.onload=a.onreadystatechange=function(){
                      var t=this.readyState;if(!t||"complete"===t||"loaded"===t)try{new MaxMilhas(config)}catch(e){}},
                      document.body.appendChild(a)}
              }();
            `
          }
        }
      />
    ]);
}