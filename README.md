# MaxMilhas - Porta dos Fundos


## 1. Atualizando links dos vídeos
### BANNER
  * Caminho: components / MainBanner / MainBanner.jsx
  * Alterar variável: videoID

### RELATED VIDEOS (Próxima missão do agente 007)
  * Caminho: container / RelatedVideosContainer / RelatedVideosContainer.json
  * Alterar variável: videoID

### EXEMPLO DE LINK:
  * https://www.youtube.com/watch?v=_L1Z1EiU2cRs_
  * videoID: L1Z1EiU2cRs

## 2. Gerando arquivo para deploy
  * Instalar o gatsby: `npm install -g gatsby-cli`
  * Acessar a pasta do projeto: `cd max-milhas-porta-dos-fundos`
  * Rodar gatsby: `gatsby build`

## 3. Publicando o site
  * Toda a pasta public deve ser publicada.


  ### Qualquer dúvida entrar em contato.
    * Wagner: 031999194904
    * Camila: 037999922114
