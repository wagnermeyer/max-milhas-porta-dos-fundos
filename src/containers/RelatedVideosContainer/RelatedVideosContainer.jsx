import React, { useEffect } from 'react'
import data from './RelatedVideosContainer.json'
import Swiper from 'swiper'
import 'swiper/css/swiper.min.css'
import './RelatedVideosContainer.sass'

import RelatedVideosContentBlock from '../../components/RelatedVideosContentBlock/RelatedVideosContentBlock'

const RelatedVideosContainer = () => {

  useEffect( () => {
    new Swiper('.swiper-videos-container', {
      spaceBetween: 0,
      slidesPerView: 'auto',
      allowTouchMove: false,
      pagination: {
        el: '.swiper-videos-pagination',
        type: 'bullets',
      },
      breakpoints: {
        500: {
          allowTouchMove: true,
        },
        1280: {
          followFinger: false
        }
      }
    })
  }, [] )

  return (
    <div className='related-videos-container'>
      <h2>Onde será a próxima missão do agente 007?</h2>
      <div className='related-videos-container__body'>
        <div className="swiper-videos-container">
          <div className="swiper-wrapper">
            {data.map((video, index) => (
              <div className="swiper-slide" key={ index }>
                <div className="related-videos-container__slide">
                  <RelatedVideosContentBlock data={ video }/>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  )
}

export default RelatedVideosContainer