import React from 'react'
import data from './CharacteristicsContainer.json'
import './CharacteristicsContainer.sass'

import CharacteristicsContentBlock from '../../components/CharacteristicsContentBlock/CharacteristicsContentBlock'

const CharacteristicsContainer = () => {
  return (
    <div className='characteristics-container'>
      <div className='container'>
        <h2>Combina, compara e mostra sempre a melhor opção de passagem</h2>
        <div className='characteristics-container__body'>
          {data.map((characteristic, index) => {
            return (
              <div className='characteristics-container__item' key={ index }>
                <CharacteristicsContentBlock data={ characteristic } />
              </div>
            )
          })}
        </div>
      </div>
    </div>
  )
}

export default CharacteristicsContainer