import React, { useEffect } from 'react'
import './SearchContainer.sass'

import securityIcon from './../../images/security.svg'
import ticketsIcon from './../../images/tickets.svg'

const SearchContainer = () => {

  useEffect( () => {

    console.log('UPDATE')

    eval(
      `
        !function() {
            var config = {
                id: "maxmilhas-widget",
                logo: false
            };
            var t="maxmilhas-widget-js";
            if(!document.getElementById(t)){
                var a=document.createElement("script");
                    a.id=t,
                    a.type="text/javascript",
                    a.src="https://widget.maxmilhas.com.br/v2/js/widget.js",
                    a.async=!0,
                    a.onload=
                    a.onreadystatechange=function(){
                      var t=this.readyState;
                      if(!t||"complete"===t||"loaded"===t)try{new MaxMilhas(config)}catch(e){}
                    },
                    document.body.appendChild(a)
            } else {
              try{new MaxMilhas(config)}catch(e){}
            }
        }();
      `
    )
  } )

  return (
    <div className='search-container'>
      <div className='container'>
        <h2>O jeito mais inteligente de buscar passagens aéreas: pesquise agora</h2>

        <div className="search-container__block">
          <div id='maxmilhas-widget'></div>
        </div>

        <div className='search-container__footer'>
          <div className='search-container__footer--security'>
            <p>Site blindado <br/> <span>e 100% seguro</span></p>
            <img src={ securityIcon } alt='Ícone de segurança'/>
          </div>
          <div className='search-container__footer--payments'>
            <img src={ ticketsIcon } alt='Ícone de tickets'/>
            <p>Parcele em <br/> <span>até 12x</span></p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default SearchContainer