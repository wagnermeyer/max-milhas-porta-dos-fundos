import React, { useEffect } from 'react'
import data from './DestinationsContainer.json'
import Swiper from 'swiper'
import 'swiper/css/swiper.min.css'
import './DestinationsContainer.sass'

import DestinationsContentBlock from './../../components/DestinationsContentBlock/DestinationsContentBlock'

const DestinationsContainer = () => {

  useEffect( () => {
    new Swiper('.swiper-container', {
      spaceBetween: 0,
      slidesPerView: 'auto',
      pagination: {
        el: '.swiper-pagination',
        type: 'bullets',
      },
      breakpoints: {
        1280: {
          followFinger: false,
        }
      }
    })
  }, [] )


  return (
    <div className='destinations-container'>
      <h2>Destinos que o Agente 007 ainda não sabe como são incríveis</h2>
      <div className='destinations-container__body'>
        <div className="swiper-container">
          <div className="swiper-wrapper">
            {data.map((destination, index) => (
              <div className="swiper-slide" key={ index }>
                <div className='destinations-container__slide'>
                  <DestinationsContentBlock data={ destination } />
                </div>
              </div>
            ))}
          </div>
          <div className="swiper-pagination"></div>
        </div>
      </div>
    </div>
  )
}

export default DestinationsContainer