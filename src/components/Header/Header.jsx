import React from 'react'
import LogoMaxMilhas from '../../images/logo-max-milhas.svg'
import './Header.sass'

const Header = () => (
  <header className='main-header'>
    <div className='container'>
      <img className='main-header__logo' src={ LogoMaxMilhas }/>
    </div>
  </header>
)

export default Header
