import React from 'react'
import LogoMaxMilhas from '../../images/logo-max-milhas.svg'
import dataContact from './FooterContact.json'
import dataSocialMidia from './FooterSocialMidia.json'
import './Footer.sass'

const Footer = () => (
  <footer className='footer-container'>
    <div className='container'>
      <div className='footer-container__wrapper'>
        <img className='footer-container__logo' src={ LogoMaxMilhas }/>

        <div className='footer-container__informations'>
          <div>
            {dataContact.map((item, index) => {
                return (
                  <div className='contact-container' key={ index }>
                    <img src={require('./../../images/' + item.icon)}  alt={ item.alt }/>
                    <a href={ item.url }>{ item.label }</a>
                  </div>
                )
            })}
          </div>

          <div className='footer-container__social-midia'>
            <p>Siga a MaxMilhas</p>
            <div className='social-midia-container'>
              {dataSocialMidia.map((item, index) => {
                return (
                  <a href={ item.url } key={ index } target="_blank">
                    <img
                      src={require('./../../images/' + item.icon)}
                      alt={ item.alt }/>
                  </a>
                )
              })}
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
)

export default Footer