import React, { useContext } from 'react'
import './RelatedVideosContentBlock.sass'
import ModalContext from '../../context/ModalContext'
import Button from './../Button/Button'

const RelatedVideosContentBlock = ({ data }) => {

  const { openModal } = useContext(ModalContext)

  const _openModal = () => {
    console.log('sds')
    openModal(true, data.videoID)
  }

  return (
    <div className='related-videos' style={{ backgroundImage: `url(${require('./../../images/' + data.image)})` }}>
      <p>{ data.body }</p>
      <div className='related-videos__button-container'>
        <Button
          label='Assistir'
          onClick={ _openModal }
        />
      </div>
    </div>
  )
}

export default RelatedVideosContentBlock