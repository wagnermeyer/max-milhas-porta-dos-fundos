import React  from 'react'
import './Button.sass'

const Button = ({ label, url, onClick }) => {
    return (
      url ?
      <a className='button'href={ url } target='_BLANK'>
        <span>
          { label }
        </span>
      </a> :
      <div
        className='button'
        onClick={ onClick }
      >
        <span>
          { label }
        </span>
      </div>
    )
}

export default Button
