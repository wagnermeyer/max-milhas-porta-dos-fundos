import React, { useState, useContext, useEffect } from 'react'
import ModalContext from '../../context/ModalContext'
import './Modal.sass'

const Modal = () => {

  const [_class, setClass] = useState('')
  const {isOpened, openModal, videoID} = useContext(ModalContext)

  const closeModal = () => {
    setClass('modal--closing')
    setTimeout( () => {
      openModal(false)
    }, 500 )
  }

  useEffect( () => {
    if (isOpened) setClass('')
  }, [isOpened] )

  return (
    isOpened &&
    <div className={`modal ${_class}`}>
      <div className='modal__content'>
        <iframe
          src={`https://www.youtube.com/embed/${videoID}?&autoplay=1`}
          frameBorder="0"
          allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
          allowFullScreen>
        </iframe>
      </div>
      <div
        className="modal__close-button"
        onClick={closeModal}
      ></div>
    </div>
  )
}

export default Modal