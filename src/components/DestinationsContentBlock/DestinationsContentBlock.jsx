import React  from 'react'
import Button from '../Button/Button'
import './DestinationsContentBlock.sass'

const DestinationsContentBlock = ({ data }) => {
    return (
      <div className='destinations-content-block'>
        <img
          className='destinations-content-block__img'
          src={require('./../../images/' + data.image)}
          alt={ data.alt }
        />
        <div className='destinations-content-block__body'>
          <div className="destinations-content-block__description">
            { data.body }
          </div>
          <Button
            label="Pesquisar passagem"
            url={ data.url }
          />
        </div>
      </div>
    )
}

export default DestinationsContentBlock
