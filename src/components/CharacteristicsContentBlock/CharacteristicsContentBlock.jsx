import React  from 'react'
import './CharacteristicsContentBlock.sass'

const CharacteristicsContentBlock = ({ data }) => {
    return (
      <div className='characteristics-content-block'>
        <img className='characteristics-content-block__icon' src={require('./../../images/' + data.icon)} alt={ data.alt }/>
        <div className='characteristics-content-block__context'>
          <h4>{ data.title }</h4>
          <p>{ data.body }</p>
        </div>
      </div>
    )
}

export default CharacteristicsContentBlock
