import React, { useContext } from 'react'
import PlayIcon from '../../images/icon-play.svg'
import MainBannerBg from '../../images/banner-img.jpg'
import ModalContext from '../../context/ModalContext'
import './MainBanner.sass'

const MainBanner = () => {

  const { openModal } = useContext(ModalContext)
  const videoID = 'bmfMDJJo0u8'

  return (
    <div className='main-banner'>
      <div className='container main-banner__height'>
        <h1 className='main-banner__text'>
          <div className="main-banner__text-line-one">Mais inteligência.</div>
          Mais economia.
        </h1>
        <div
          className='video-button'
          onClick={ () => openModal(true, videoId) }
        >
          <img className='video-button__icon' src={ PlayIcon }/>
          <div
            className='video-button__label'
          >Assista ao vídeo</div>
        </div>
        <img className='main-banner__bg' src={ MainBannerBg }/>
      </div>
    </div>
  )
}

export default MainBanner