import React, { useState } from 'react'
import ModalContext from '../context/ModalContext'

const ModalProvider = ({children}) => {

  const [isOpened, setIsOpened] = useState(false)
  const [videoID, setVideoID] = useState('')

  const openModal = (boolean, videoID) => {
    setIsOpened(boolean)
    setVideoID(videoID)
  }

  return (
    <ModalContext.Provider value={ {
      isOpened,
      openModal: (boolean, videoID) => openModal(boolean, videoID),
      videoID}
    }>
      {children}
    </ModalContext.Provider>
  )
}

export default ModalProvider