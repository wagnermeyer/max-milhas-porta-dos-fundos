import React from "react"
import './index.sass'

// Components - - - -
import Header from '../components/Header/Header'
import MainBanner from '../components/MainBanner/MainBanner'
import Footer from '../components/Footer/Footer'
import Modal from '../components/Modal/Modal'

// Containers - - - -
import CharacteristicsContainer from '../containers/CharacteristicsContainer/CharacteristicsContainer'
import SearchContainer from '../containers/SearchContainer/SearchContainer'
import DestinationsContainer from '../containers/DestinationsContainer/DestinationsContainer'
import RelatedVideosContainer from '../containers/RelatedVideosContainer/RelatedVideosContainer'

// Providers - - - - -
import ModalProvider from '../providers/ModalProvider'

const IndexPage = () => (
  <React.Fragment>
    <ModalProvider>
      <Header/>
      <MainBanner/>
      <CharacteristicsContainer/>
      <SearchContainer/>
      <DestinationsContainer/>
      <RelatedVideosContainer/>
      <Footer/>
      <Modal/>
    </ModalProvider>
  </React.Fragment>
)

export default IndexPage
